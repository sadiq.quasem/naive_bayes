from train import *

def loadCsv_test():
    filename = 'pima_indians_diabetes.csv'
    dataset = loadCsv(filename)
    print("Loaded data file {0} with {1} rows".format(filename, len(dataset)))

def splitDataset_test():
    dataset = [[1], [2], [3], [4], [5], [6], [7], [8], [9], [10]]
    splitRatio = 0.8
    train, test = splitDataset(dataset, splitRatio)
    print('Split {0} rows into train with {1} and test with {2}'.format(len(dataset), train, test))

def separateByClass_test():
    dataset = [[1,20,1], [2,21,0], [3,22,1]]
    separated = separateByClass(dataset)
    print('Separated instances: {0}').format(separated)

def mean_stdev_test():
    numbers = [1,2,3,4,5]
    print('Summary of {0}: mean={1}, stdev={2}').format(numbers, mean(numbers), stdev(numbers))

def summarize_test():
    dataset = [[1,20,0], [2,21,1], [3,22,0]]
    summary = summarize(dataset)
    print('Attribute summaries: {0}').format(summary)

def summarizeByClass_test():
    dataset = [[1,20,1], [2,21,0], [3,22,1], [4,22,0]]
    summary = summarizeByClass(dataset)
    print('Summary by class value: {0}').format(summary)

def calculateProbability_test():
    x = 71.5
    mean = 73
    stdev = 6.2
    probability = calculateProbability(x, mean, stdev)
    print('Probability of belonging to this class: {0}').format(probability)

def calculateClassProbabilities_test():
    summaries = {0:[(1, 0.5)], 1:[(20, 5.0)]}
    inputVector = [1.1, '?']
    probabilities = calculateClassProbabilities(summaries, inputVector)
    print('Probabilities for each class: {0}').format(probabilities)

def predict_test():
    summaries = {'A':[(1, 0.5)], 'B':[(20, 5.0)]}
    inputVector = [1.1, '?']
    result = predict(summaries, inputVector)
    print('Prediction: {0}').format(result)

def getPredictions_test():
    summaries = {'A':[(1, 0.5)], 'B':[(20, 5.0)]}
    testSet = [[1.1, '?'], [19.1, '?']]
    predictions = getPredictions(summaries, testSet)
    print('Predictions: {0}').format(predictions)

def getAccuracy_test():
    testSet = [[1,1,1,'a'], [2,2,2,'a'], [3,3,3,'b']]
    predictions = ['a', 'a', 'a']
    accuracy = getAccuracy(testSet, predictions)
    print('Accuracy: {0}').format(accuracy)
