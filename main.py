import csv
import random
import math
import src.train as train

def main():
	filename = 'data/pima_indians_diabetes.csv'
	splitRatio = 0.67
	dataset = train.loadCsv(filename)
	trainingSet, testSet = train.splitDataset(dataset, splitRatio)
	print('Split {0} rows into train={1} and test={2} rows'.format(len(dataset), len(trainingSet), len(testSet)))
	# prepare model
	summaries = train.summarizeByClass(trainingSet)
	# test model
	predictions = train.getPredictions(summaries, testSet)
	accuracy = train.getAccuracy(testSet, predictions)
	print('Accuracy: {0}%'.format(accuracy))

main()
